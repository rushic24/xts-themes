# MacOS 10 Themes for Xfce4 Themes Switcher

Three nice MacOS themes for [Xfce4 Themes Switcher](https://gitlab.com/linux-stuffs/xfce4-theme-switcher).

This themes require Xfce4, Xfce4 Theme Switcher, Plank and Whisker menu.

## macOS Catalina

![macOS Catalina](img-git/macOS-Catalina.png)

## macOS Sierra

![macOS-Sierra](img-git/macOS-Sierra.png)

## OS X Yosemite

![OS-X-Yosemite](img-git/OS-X-Yosemite.png)

## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/xts-macos-theme/):
```
git clone https://aur.archlinux.org/xts-macos-theme.git
cd xts-macos-theme/
makepkg -sci
```

### From the binary package (Debian-based Linux distributions):

Download the latest xts-macos-theme package from [here](https://github.com/entexsoft/deb-packages/tree/master/packages).

After download you need then run this command (like root):

```
apt install ./xts-macos-theme*.deb
```


### Install from source:
Unpack the source package and run command (like root):
```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build own ArchLinux package
You need these packages for building the ArchLinux package:
```
base-devel git wget yajl
```

Run command:
```
make build-arch
```

### Install from *.pkg.tar.xz package:
Run command (like root):
```
pacman -U distrib/xts-macos-theme*.pkg.tar.xz
```

### Build Debian own package
You need these packages for building the Debian package:
```
build-essential make fakeroot util-linux debianutils lintian dpkg coreutils
```

Run command (like root):
```
make build-deb
```
### Install from *.deb package:
```
apt install ./distrib/xts-macos-theme*.deb
```
