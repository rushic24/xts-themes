# Windows Server 2003 Theme for Xfce4 Themes Switcher

Windows Server 2003 theme for [Xfce4 Themes Switcher](https://gitlab.com/linux-stuffs/xfce4-theme-switcher).

This themes require Xfce4, Xfce4 Theme Switcher and Whisker menu.

## Windows Server 2003

![Windows-Server-2003](img-git/Windows-Server-2003.png)

## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/xts-windows-server-2003-theme/):
```
git clone https://aur.archlinux.org/xts-windows-server-2003-theme.git
cd xts-windows-server-2003-theme/
makepkg -sci
```

### From the binary package (Debian-based Linux distributions):

Download the latest xts-windows-server-2003-theme package from [here](https://github.com/entexsoft/deb-packages/tree/master/packages).

After download you need then run this command (like root):

```
apt install ./xts-windows-server-2003-theme*.deb
```


### Install from source:
Unpack the source package and run command (like root):
```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build own ArchLinux package
You need these packages for building the ArchLinux package:
```
base-devel git wget yajl
```

Run command:
```
make build-arch
```

### Install from *.pkg.tar.xz package:
Run command (like root):
```
pacman -U distrib/xts-windows-server-2003-theme*.pkg.tar.xz
```

### Build Debian own package
You need these packages for building the Debian package:
```
build-essential make fakeroot util-linux debianutils lintian dpkg coreutils
```

Run command (like root):
```
make build-deb
```
### Install from *.deb package:
```
apt install ./distrib/xts-windows-server-2003-theme*.deb
```
