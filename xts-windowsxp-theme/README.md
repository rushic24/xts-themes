# Windows XP Theme for Xfce4 Themes Switcher

Windows XP themes for [Xfce4 Themes Switcher](https://gitlab.com/linux-stuffs/xfce4-theme-switcher).

This themes require Xfce4 and Xfce4 Theme Switcher.

![Windows-XP](img-git/Windows-XP.png)


## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/xts-windowsxp-theme/):
```
git clone https://aur.archlinux.org/xts-windowsxp-theme.git
cd xts-windowsxp-theme/
makepkg -sci
```

### From the binary package (Debian-based Linux distributions):

Download the latest xts-windows10-theme package from [here](https://github.com/entexsoft/deb-packages/tree/master/packages).

After download you need then run this command (like root):

```
apt install ./xts-windowsxp-theme*.deb
```


### Install from source:
Unpack the source package and run command (like root):
```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build own ArchLinux package
You need these packages for building the ArchLinux package:
```
base-devel git wget yajl
```

Run command:
```
make build-arch
```

### Install from *.pkg.tar.xz package:
Run command (like root):
```
pacman -U distrib/xts-windowsxp-theme*.pkg.tar.xz
```

### Build Debian own package
You need these packages for building the Debian package:
```
build-essential make fakeroot util-linux debianutils lintian dpkg coreutils
```

Run command (like root):
```
make build-deb
```
### Install from *.deb package:
```
apt install ./distrib/xts-windowsxp-theme*.deb
```
